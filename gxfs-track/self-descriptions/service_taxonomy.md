# Service Taxonomy

Service Taxonomy defines sub-classes for each top-level element of Gaia-X Conceptual Model:

- [Participant](#participant)
- [Service Offering](#service offering)
- [Resource](#resource)

## Participant

```mermaid
classDiagram

Participant <|-- Provider
Participant <|-- Federator
Participant <|-- Consumer

Provider "1" <-- ServiceOffering : isOfferedBy
Provider "1" <-- Resource : isOperatedBy
```

## Service Offering
### Service Composition

```mermaid
classDiagram

Provider "1" <-- ServiceOffering : "isOfferedBy"

ServiceOffering --> "1" ServiceOffering : "dependsOn"
ServiceOffering o-- "*" Resource : "isComposedBy"

```

### Service Classes 

```mermaid
classDiagram

ServiceOffering <|-- Software
ServiceOffering <|-- Platform
ServiceOffering <|-- Infrastructure

Infrastructure <|-- Compute
Infrastructure <|-- Storage
Infrastructure <|-- Network

Platform <|-- K8saaS

Software <|-- DataServiceOffering

Compute <|-- BareMetal Service Offering

```

### Data Service Offering

```mermaid
classDiagram

ServiceOffering <|-- Software
Software <|--  DataServiceOffering
DataServiceOffering --> "1..*" DataResource

```


### Bare Metal Service Offering

```mermaid
classDiagram

Infrastructure <|--  BareMetalServiceOffering
BareMetalServiceOffering --> "1..*" BareMetalNode
```


## Resource


```mermaid
classDiagram

Resource o-- "*" Resource : "isComposedBy"

Resource <|-- DataResource
Resource <|-- SoftwareResource
Resource <|-- Node
Resource <|-- Interconnection
Resource <|-- NetworkingDevice

```

### Node

```mermaid
classDiagram

Node <|-- BareMetalNode
Node <|-- VirtualNode
Node <|-- Container

BareMetalNode --> "*" CPU
BareMetalNode --> "*" GPU
BareMetalNode --> "*" HardDrive
```


### Interconnection

```mermaid
classDiagram

Interconnection --> "2" Node : connects

Interconnection --> "0..1" PhysicalMedium : consists_of
Interconnection --> Connection : consists_of
Interconnection --> Route : consists_of
```
