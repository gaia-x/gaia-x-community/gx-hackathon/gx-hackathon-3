# Self Description Attributes - Overview

Version: 24/03/2022 07:07:20


## Outline

- [BareMetalNode](#baremetalnode)
- [BareMetalServiceOffering](#baremetalserviceoffering)
- [Connection](#connection)
- [Cpu](#cpu)
- [DataAccessStandard](#dataaccessstandard)
- [DataModellingStandard](#datamodellingstandard)
- [DataResource](#dataresource)
- [DataServiceOffering](#dataserviceoffering)
- [Gpu](#gpu)
- [HardDrive](#harddrive)
- [HardwareSpec](#hardwarespec)
- [Infrastructureserviceoffering](#infrastructureserviceoffering)
- [Interconnection](#interconnection)
- [K8snode](#k8snode)
- [K8saas](#k8saas)
- [NetworkingDevice](#networkingdevice)
- [Node](#node)
- [Participant](#participant)
- [PhysicalMedium](#physicalmedium)
- [Provider](#provider)
- [Resource](#resource)
- [Route](#route)
- [ServiceOffering](#serviceoffering)
- [Ssd](#ssd)
- [Standard](#standard)
- [TrustedCloudProvider](#trustedcloudprovider)
- [TrustedCloudServiceOffering](#trustedcloudserviceoffering)
- [TrustedCloudServiceOfferingArchitecture](#trustedcloudserviceofferingarchitecture)
- [TrustedCloudServiceOfferingCertificates](#trustedcloudserviceofferingcertificates)
- [TrustedCloudServiceOfferingContracts](#trustedcloudserviceofferingcontracts)
- [TrustedCloudServiceOfferingDataProtection](#trustedcloudserviceofferingdataprotection)
- [TrustedCloudServiceOfferingInteroperability](#trustedcloudserviceofferinginteroperability)
- [TrustedCloudServiceOfferingOperativeProcesses](#trustedcloudserviceofferingoperativeprocesses)
- [TrustedCloudServiceOfferingSecurity](#trustedcloudserviceofferingsecurity)
- [TrustedCloudServiceOfferingSubCompanies](#trustedcloudserviceofferingsubcompanies)


# BareMetalNode
```mermaid
classDiagram

Node <|-- BareMetalNode
BareMetalNode --> "*" CPU
BareMetalNode --> "*" GPU
BareMetalNode --> "*" HardDrive

```

 **Super class**: [Node](#node)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasCPU | xsd:string |  0 |  unlimited | A Gaia-x reference Identifier | gax:Intel-Xeon-Platinum-8280 | 
| hasGPU | xsd:string |  0 |  unlimited | A Gaia-x reference Identifier | gax:NVIDIA-A10 | 
| hasRamsize | gax-core:Measure |  0 |  unlimited | a structured object of type measure, e.g. measure:value=950 and measure:unit=GB | 950 GB | 
| hasHarddrive | xsd:string |  0 |  unlimited | a reference ID to that hard drive | gax:Intel-SSD-DC-P4610 | 
| hasNIC | gax-core:Measure |  0 |  unlimited | a structured object of type measure, e.g. measure:value=10 and measure:unit=GBase-T | 10 GBase-T | 



# BareMetalServiceOffering
```mermaid
classDiagram

ServiceOffering <|-- Infrastructure 
Infrastructure <|--  BareMetalServiceOffering
BareMetalServiceOffering --> "1..*" BareMetalNode


```

 **Super class**: [ServiceOffering](#serviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasBareMetalNode | gax-resource:BareMetalNode | 1 |  unlimited | List of bare metal nodes associated that offering | gax:CPU-Node-1 | 



# Connection

```mermaid
classDiagram

Interconnection --> Connection : consists_of

```

 **Super class**: [Resource](#resource)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasConnectionPointA | xsd:string | 1 | 1 | The ID of the source of the connection. | a MAC address, a VLAN ID | 
| hasConnectionPointZ | xsd:string | 1 | 1 | The ID of the destination of the connection. | a MAC address, a VLAN ID | 
| hasBandwidth | gax-core:Measure | 1 | 1 | Contractual bandwidth defined in the service level agreement (SLA). Bandwidth is usually measured in dimension of bits per second. | 500 Gbps | 
| hasLatency | gax-core:Measure | 1 | 1 | Contractual latency defined in the SLA. If not specified, then best effort is assumed. Latency is usually measured in dimension of time. | 10 seconds | 
| hasAvailability | gax-core:Measure | 1 | 1 | Contractual availability of connection defined in the SLA agreement. If not specified, then best effort is assumed. Availability is measured in the pseudo-unit "percent". | 99 percent | 
| hasPacketLoss | gax-core:Measure | 1 | 1 | Contractual packet loss of connection defined in the SLA agreement. If not specified, then best effort is assumed. PackageLoss s measured in the pseudo-unit "percent"- | 0.00002 % | 
| hasJitter | gax-core:Measure | 1 | 1 | Contractual jitterdefined in the SLA. If not specified, then best effort is assumed. Jitter is measured in dimension of time. | 0.01 miliseconds | 
| hasIntermediateConnectionPointsN | xsd:string | 0 | 1 | all relevant interconnection points on the way between the source and destination. | a MAC address, a VLAN ID | 
| hasConnectionType | xsd:string | 1 |  unlimited | the supported types of connection, preferably specified as a controlled vocabulary entry | ethernet unicast, multicast, broadcast support | 



# Cpu
```mermaid
classDiagram

HardwareSpec <|-- CPU
```

 **Super class**: [HardwareSpec](#hardwarespec)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasNumberOfCores | xsd:integer | 1 | 1 | Number of Cores of the CPU | 4, 6, 8, 12, 24 | 
| hasNumberOfThreads | xsd:integer | 1 | 1 | Number of Threads of the CPU | 8, 12, 24 | 
| hasFrequency | gax-core:Measure | 1 | 1 | Frequency of the CPU | 3.0 GHz, 3.2 GHz | 
| hasBoostFrequency | gax-core:Measure | 1 | 1 | Boost frequency of the CPU | 4,0 GHz | 
| hasCache | gax-core:Measure | 1 | 1 | Cache of the CPU | 38.0 MB | 
| hasAllowedSocket | xsd:string | 1 | 1 | Socket the CPU fits into | FCLGA3647 | 



# DataAccessStandard
```mermaid
classDiagram

Standard <|-- DataAccessStandard

```

 **Super class**: [gax-core:Standard](#gax-core:standard)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasAPIDescription | xsd:anyURI |  0 |  unlimited | Provides a link to API description of the standard. | https://www.mystandard.com/openapi | 



# DataModellingStandard
```mermaid
classDiagram

    Standard <|-- DataModelingStandard
    
```

 **Super class**: [Standard](#standard)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | modelType | xsd:string |  0 |  unlimited | The type of data model (conceptual, logical, physical) | conceptual, logical, physical | 



# DataResource
```mermaid
classDiagram

Resource <|--   DataResource

```

 **Super class**: [Resource](#resource)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasTags | xsd:string |  0 |  unlimited | Tags and Keywords describing this data resource. | air quality, time series data, measurements | 
| hasCreationTime | xsd:dateTimeStamp | 0 | 1 | Timestamp the service has been created. | 2021-07-17T00:31:30Z | 
| hasModificationDate | xsd:dateTimeStamp | 0 | 1 | Timestamp the service has been created. | 2021-07-17T00:31:30Z | 
| hasUpdateInterval | xsd:string |  0 |  unlimited | Interval in which new data points are added to the data set. | P1D | 
| hasGeographicCoverage | xsd:string | 0 | 1 | Geographic region covered by the dataset. |  | 
| hasSpatialResolution | xsd:string | 0 | 1 | The spatial resolution of the dataset. |  | 
| hasTemporalCoverage | xsd:string | 0 | 1 | The time period covered by the data set. | 2021-07-17T00:31:30Z/2022-07-17T00:31:30Z | 
| hasTemporalResolution | xsd:string | 0 | 1 | The time interval between data points, if this data set provides time series data | PT1S | 



# DataServiceOffering
```mermaid
classDiagram

ServiceOffering <|-- Software
Software <|--  DataServiceOffering
DataServiceOffering --> "1..*" DataResource

```

 **Super class**: [Software](#software)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasLicense | xsd:string | 0 | 1 | Reference to the license of the service, preferably a controlled vocabulary entry referenced by URI | Public-Domain, CC-BY | 
| hasCopyrightHolder | vcard:Agent | 1 | 1 | Reference to the author or copyright holder | (a structured object having, e.g., the attributes vcard:given-name and vcard:hasEmail) | 
| hasType | xsd:string | 1 | 1 | Type of the data asset, which helps discovery.  Preferably a controlled vocabulary entry referenced by URI | dataset, container | 
| hasCreationTime | xsd:dateTimeStamp | 1 | 1 | Timestamp the service has been created. | 2021-07-17T00:31:30Z | 
| hasStandardConformity | gax-core:Standard | 1 |  unlimited | Provides information about applied standards. | (reference to standard | 
| hasEndpoint | gax-service:Endpoint | 1 |  unlimited | Endpoint through which the DataServiceOffering can be accessed | (reference to endpoint) | 
| hasDataResource | gax-service:DataResource | 1 |  unlimited | Data resource(s) that the data service makes available | One or more data-resource instances or references | 



# Gpu

```mermaid
classDiagram

HardwareSpec <|-- GPU

```

 **Super class**: [HardwareSpec](#hardwarespec)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasMemory | xsd:string | 1 | 1 | Memory of the GPU | 24GB DDR6 | 
| hasConnection | xsd:string | 1 | 1 | Interconnection of the GPU | PCIe Gen4: 64GB/s | 



# HardDrive
```mermaid
classDiagram

HardwareSpec <|-- HardDrive

```

 **Super class**: [HardwareSpec](#hardwarespec)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasSize | gax-core:Measure | 1 | 1 | The size of that hard drive. | 1600 GB | 
| hasType | xsd:string | 1 | 1 | The type of that hard drive. | SSD, HDD, M.2 | 



# HardwareSpec
```mermaid
classDiagram

HardwareSpec <|-- CPU
HardwareSpec <|-- GPU
HardwareSpec <|-- HardDrive

```

 **Super class**: 

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasProductID | xsd:string | 1 | 1 | ID of the Hardware Spec | 6CX68AV | 
| hasTitle | xsd:string | 1 | 1 | Title of the hardware resource | Xeon Platinum 8280 | 
| hasManufacturer | xsd:string | 1 | 1 | References a manufacturer of the GPU | NVIDIA | 



# Infrastructureserviceoffering
# Service Offering - Infrastructure Service Offering

```mermaid
classDiagram

ServiceOffering <|-- InfrastructureServiceOffering 
```

 **Super class**: [ServiceOffering](#serviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | dependsOn | xsd:string | 1 |  unlimited | Name of that service | gax-service:DataCenter | 
| isComposedBy | xsd:string | 1 |  unlimited | Name of that service | gax-service:Networking, gax-service:Compute, gax-service:Storage | 



# Interconnection

```mermaid
classDiagram

Resource <|-- Interconnection

Interconnection --> "2" Node : connects

Interconnection --> PhysicalMedium : consists_of
Interconnection --> Connection : consists_of
Interconnection --> Route : consists_of

```

 **Super class**: [Resource](#resource)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasPhysicalMedium | gax-resource:PhysicalMedium | 0 | 1 | a part of interconnection resource describing L1 connection.  Preferably a controlled vocabulary entry defined by a URI. | fiber, WLAN, 5G | 
| hasConnection | gax-resource:Connection | 0 | 1 | a part of interconnection resource describing L2 connection.  Preferably a controlled vocabulary entry defined by a URI | ethernet | 
| hasRoute | gax-resource:Route | 0 | 1 | a part of interconnection resource describing L3 connection.  Preferably a controlled vocabulary entry defined by a URI | TCP/IP | 
| Location | xsd:string | 1 | 1 | Location of the interconnection | Germany | 
| hasJurisdiction | xsd:string | 1 | 1 | Jurisdiction of the interconnection | Germany | 



# K8snode

```mermaid
classDiagram

KubernetesNode <|-- BareMetalNode
```
 **Super class**: [Node](#node)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasName | xsd:string | 1 | 1 | Descriptive name of the k8s node | CPU Node 1 | 
| Location | xsd:string | 1 | 1 | Location of the node | Germany | 
| hasJurisdiction | xsd:string | 1 | 1 | Jurisdiction of the node | Germany | 



# K8saas

```mermaid
classDiagram

ServiceOffering <|-- Platform 
Platform <|-- K8saaS
```

 **Super class**: [serviceOffering](#serviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasServiceTitle | xsd:string | 1 | 1 | Name of the service | K8s service | 
| hasServiceDescription | xsd:string | 1 | 1 | Slightly specific description of this k8s service | K8s service, that provides a container platform for the user | 
| hasKeyword | xsd:string | 1 | 1 | Keywords that describe / tag the service. | K8s, Managed K8s | 
| isDefinedBy | gax-participant:Provider | 1 | 1 | The provider of the service | gax:Company-1 | 
| hasProvisionType | gax-core:meaningfulString | 1 | 1 | Provision type of the service | Hybrid, gax:PrivateProvisioning | 
| hasServiceModel | gax-core:meaningfulString | 1 | 1 | Service Model for the service | IaaS, PaaS | 
| did | did:example | 1 | 1 | DID of the service | did:example:123456789abcdefghi | 
| providedBy | did:example | 1 | 1 | DID of the provider of the service | did:example:abcdefghi123456789 | 



# NetworkingDevice

```mermaid
classDiagram

Resource <|-- NetworkingDevice
```

 **Super class**: [Resource](#resource)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasManagementPort | xsd:string | 1 | 1 | A dedicated port for management tasks. | no, yes | 
| hasConsolePort | xsd:string | 1 | 1 | A dedicated port for console tasks. | no, yes | 
| hasPortCapacity_A | xsd:float | 1 | 1 | The assigned capacity of ports. | 10GE, 100GE | 
| hasPortCapacity_A_Count | xsd:integer | 1 | 1 | The assigned number of ports. | 10, 1 | 
| hasRedundantPowerSupply | xsd:string | 1 | 1 | Availability of redundant power supply for the cases of emergency. | yes, no | 
| hasRAMSize | gax-core:Measure | 1 | 1 | Ram size of the networking device | 2Gb | 
| hasCPUCount | gax-core:Measure | 1 | 1 | Number of available CPUs. | 1 | 
| hasType | xsd:string | 1 | 1 | Type of networking device | switch, router, repetear | 
| hasSupportedProtocols | xsd:string | 1 | 1 | List of  supported protocols among used layers should be specified. | IP, IRP | 
| hasNetworkAdress | xsd:float | 1 |  unlimited | IP address of the netowrking device | 192.168.24.2/32 | 



# Node
```mermaid
classDiagram

Node <|-- BareMetalNode
Node <|-- VirtualNode
Node <|-- Container
```

 **Super class**: [Resource](#resource)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasNodeName | xsd:string | 1 | 1 | Name of this node | Master Node 102 | 



# Participant

```mermaid
classDiagram

Participant <|-- Provider
Participant <|-- Federator
Participant <|-- Consumer

Provider <|-- TrustedCloudProvider
```




 **Super class**: 

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasLegallyBindingName | xsd:string | 1 | 1 | The full legal name of the organization. | ACME GmbH, Foo LLC | 
| hasLegallyBindingAddress | vcard:Address | 1 | 1 | The full legal address of the organization. | (a structured object having, e.g., the attributes vcard:street-address,vcard:locality and vcard:country-name) | 



# PhysicalMedium

```mermaid
classDiagram

Interconnection --> PhysicalMedium : consists_of

```




 **Super class**: [Resource](#resource)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasLocation | xsd:string | 2 | 2 | References two locations that require a connection | (a reference to Node object) | 
| hasType | xsd:string | 1 | 1 | Type of technology used for the physical connection.  Preferably a controlled vocabulary entry defined by a URI | Copper cable, 5G | 



# Provider

```mermaid
classDiagram

Participant <|-- Provider
Provider "1" <-- ServiceOffering : isDefinedBy
Provider "1" <-- Resource : isOperatedBy

```




 **Super class**: [Participant](#participant)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasLegalForm | xsd:string | 1 | 1 | Legal form of provider. | GmbH, LLC | 
| hasJurisdiction | xsd:string | 1 | 1 | Jurisdiction of provider. The law under which law an organization is incorporated. | Germany | 
| hasVATnumber | xsd:string | 1 | 1 | A number given by the government to identify this provider when making VAT payments. | "DE 129515865" (in Germany) | 
| hasLegalRegistrationNumber | xsd:string | 1 | 1 | Legal registration number of provider. A unique number of digits and/or characters given by government to identify an organization. | HRB 12 | 
| hasWebAddress | xsd:anyURI | 1 |  unlimited | A web address of the organization | https://gaia-x.eu/ | 
| hasIndividualContactLegal | vcard:Agent | 1 |  unlimited | A contact person within an organization to contact for legal purposes. | (a structured object having, e.g., the attributes vcard:given-name and vcard:hasEmail) | 
| hasIndividualContactTechnical | vcard:Agent | 1 |  unlimited | A contact person within an organisation to contact for technical purposes | (a structured object having, e.g., the attributes vcard:given-name and vcard:hasEmail) | 
| defines | gax-service:ServiceOffering |  0 |  unlimited | A list of Service Offerings this provider defined. | gax:ServiceOffering-1 | 
| operates | gax-resource:Resource |  0 |  unlimited | A list of resources this provider is operating. | gax:Resource-1, gax:Resource-2 | 



# Resource

```mermaid
classDiagram

Resource o-- Resource
Provider "1" --> "*" Resource : operates
ServiceOffering o-- Resource
Resource <|-- DataResource
Resource <|-- SoftwareResource
Resource <|-- Node
Resource <|-- Interconnection
Resource <|-- NetworkingDevice

```

 **Super class**: 

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasName | xsd:string | 1 | 1 | Name of resource. | Example Resource | 
| hasDescription | xsd:string | 1 | 1 | A more detailed description of resource. | Example Resource placed somewhere in Europe | 
| location | xsd:string | 1 | 1 | Physical location of resource. | (a structured object of type Location) | 
| hasJurisdiction | xsd:string | 1 | 1 | Jurisdiction of resource | (a structured object of type Location) | 
| isOperatedBy | gax-participant:Provider | 1 | 1 | Id of provider who is operation the resource | gax:Company-1 | 
| isComposedBy | gax-resource:Resource |  0 |  unlimited | Sub-resources this resource is composed by | gax:Resource-1 | 



# Route
```mermaid
classDiagram

class route 
```

 **Super class**: [Resource](#resource)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | connectedNetwork | xsd:decimal | 1 | 1 | autonomous system (AS) number (ASN) should be provided | 200, 714 | 
| prefixSet | xsd:string | 1 | 1 | CIDR Provider Identifier | 200, 714 | 



# ServiceOffering

```mermaid
classDiagram

Provider <-- ServiceOffering : "isOfferedBy"

ServiceOffering --> ServiceOffering : "dependsOn"
ServiceOffering o-- Resource : "isComposedBy"

ServiceOffering <|-- Software
ServiceOffering <|-- Platform
ServiceOffering <|-- Infrastructure
```




 **Super class**: 

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasServiceTitle | xsd:string | 1 | 1 | Name of the service | Image classification ML service | 
| description | xsd:string | 1 | 1 | A description in natural language | An ML service for training, deploying and improving image classifiers. | 
| keyword | xsd:string | 1 | 1 | Keywords that describe / tag the service. | ML, Classification | 
| isDefinedBy | gax-participant:Provider | 1 | 1 | The provider of the service | gax:Company-1 | 
| hasProvisionType | xsd:string | 1 | 1 | Provision type of the service | Hybrid, gax:PrivateProvisioning | 
| dependsOn | gax-service:ServiceOffering |  0 |  unlimited | Optional sub service offerings | gax:Storage | 
| isComposedBy | gax-resource:Resource |  0 |  unlimited | Optional resource used by that service offering | gax:DataCenter | 



# Ssd

```mermaid
classDiagram

HardDrive <-- SSD
```

 **Super class**: [Resource](#resource)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasProductID | xsd:string | 1 | 1 | ID of the Hardware Spec | SSDPE2KE016T801 | 
| hasTitle | xsd:string | 1 | 1 | Title of the SSD | SSD DC P4610 | 
| hasManufacturer | xsd:string | 1 | 1 | References a manufacturer of the GPU | Intel | 
| hasCapacity | gax-core:Measure | 1 | 1 | Capacity of the SSD | 1.600 GB | 
| hasFormFactor | gax-core:Measure | 1 | 1 | Form factor of the SSD | 2,5 Inches | 
| hasReadSpeed | gax-core:Measure | 1 | 1 | Read speed of the SSD | 3200 MB | 
| hasWriteSpeed | gax-core:Measure | 1 | 1 | Write speed of the SSD | 2100 MB | 
| hasProtocol | xsd:string | 1 | 1 | Protocol used by the SSD | NVME 1.2 | 



# Standard

```mermaid
classDiagram

    Standard <|-- DataAccessStandard
    Standard <|-- DataModelingStandard
    Standard <|-- Certification
    
```

 **Super class**: 

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasTitle | xsd:string | 1 | 1 | Name of the standard. | ISO10303-242:2014 | 
| hasStandardReference | xsd:anyURI | 1 |  unlimited | Provides a link to schemas or details about applied standards. | https://www.iso.org/standard | 
| hasPublisher | xsd:string | 0 | 1 | Publisher of the standard. | International Organization for Standardization | 



# TrustedCloudProvider
```mermaid
classDiagram

Provider <|-- TrustedCloudProvider
```

 **Super class**: [Provider](#provider)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | registrationDate | xsd:dateTimeStamp | 1 | 1 | Date of the legal registration. | 2021-10-18T12:00:00+01:00 | 
| isSME | xsd:boolean | 1 | 1 | Is the company a "small or medium sized company". | yes, no | 
| mainAddress | vcard:Address | 1 | 1 | Address of the head office |  | 
| hasMainContact | vcard:Agent | 1 | 1 | Main contact of the organisation | (a structured object having, e.g., the attributes vcard:given-name and vcard:hasEmail) | 
| hasDataProtectionContact | vcard:Agent | 1 | 1 | A contact within an organisation to contact for data protection purposes. | (a structured object having, e.g., the attributes vcard:given-name and vcard:hasEmail) | 
| employeeCount | xsd:string | 1 | 1 | Employee count of the organisation. | 50, No specification | 
| employeeCountForCloudServices | xsd:string | 1 | 1 | Employee count of the organisation in the cloud service area. | 50, No specification | 
| salesVolume | xsd:string | 1 | 1 | Sales volume of the organisation. | 50.000.000, No specification | 
| hasCategory | xsd:enum | 1 | 1 | Specific category of the organisation. | Reseller, ISV, CSP, Others | 
| countPublicCloudServices | xsd:string | 1 | 1 | Publicly offered cloud services. |  | 
| experienceCloudServices | xsd:string | 1 | 1 | Experience / knowledge of the organisation in the cloud service business. | <1 year, 1-5 year, >5 year, Others | 
| isAuditableAfterUserRequest | xsd:boolean | 1 | 1 | Is it possible for the user to request an audit? | yes, no | 
| wasAlreadyAudited | xsd:string | 1 | 1 | Was the organisation already audited? |  | 
| isAuditableRelatedToDataProtection | xsd:string | 1 | 1 | Experience / knowledge of the organisation in the cloud service business. | yes, no, No specification | 



# TrustedCloudServiceOffering

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingArchitecture
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingCertificate
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingContract
TrustedCloudServiceOffering <|-- TrustedCloudServiceDataProtection
TrustedCloudServiceOffering <|-- TrustedCloudServiceInteroperability
TrustedCloudServiceOffering <|-- TrustedCloudServiceOperativeProcesses
TrustedCloudServiceOffering <|-- TrustedCloudServiceSecurity
TrustedCloudServiceOffering <|-- TrustedCloudServiceSubCompanies


```

 **Super class**: [ServiceOffering](#serviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasServiceTitle | xsd:string | 1 | 1 | Name of the service | Image classification ML service | 
| hasServiceLogo | xsd:base64Binary |  0 |  unlimited | PDF / JPG / PNG file to show the logo of the service. | logo.png | 
| hasProvisionType | xsd:string | 1 | 1 | Provision type of the service | Hybrid, gax:PrivateProvisioning | 
| hasServiceModel | xsd:string | 1 | 1 | Service Model for the service | IaaS, PaaS | 
| hasWebsite | xsd:anyURI | 1 | 1 | Web address for the service presentation. | http://myservice.com | 
| description | xsd:string | 1 | 1 | A description in natural language | An ML service for training, deploying and improving image classifiers. | 
| hasServiceShortDescription | xsd:string | 1 | 1 | A short description in natural language to display on the query site. | An ML service for training, deploying and improving image classifiers. | 
| keyword | xsd:string | 1 | 1 | Keywords that describe / tag the service. | ML, Classification | 
| isDefinedBy | gax-participant:Provider | 1 | 1 | The provider of the service | gax:Company-1 | 



# TrustedCloudServiceOfferingArchitecture

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingArchitecture

```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasGuaranteedTenantSeparation | xsd:string | 1 | 1 | Is a continuous tenant separation guaranteed? | No statement, Not guaranteed, Guaranteed, Guaranteed and can be proven | 
| hasTenantSeparation | xsd:string | 1 | 1 | How does the tenant separation work? | Freetext | 
| hasDefinitionOfScalingTypesOfTheInfrastructure | xsd:string | 1 | 1 | Are the scaling types and factors of the technical infrastructure defined? | No statement, No defined, Scaling types are defined, Scaling types are defined and assured by contract | 
| hasDeclarationsAboutTheScalingTypes | xsd:string | 1 | 1 | Has additional declarations about the scaling types | Freetext | 



# TrustedCloudServiceOfferingCertificates

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingCertificates
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasCertificateName | xsd:string | 1 | 1 | Relevant certificate for the service | ISO XYZ | 
| hasCertificateType | xsd:string | 1 | 1 | Type of the certificates | BSI IT-Grundschutz, ISO 27001, Trust in Cloud | 
| hasDescriptionOfTestScope | xsd:string | 1 | 1 | Description of the scope of testing for this service | Freetext | 
| hasCertificateAuthority | xsd:string | 1 | 1 | Certificate authority for this service or its certificate | TÜV Süd | 
| hasCertificateDocument | xsd:string | 1 | 1 | Document that contains a certificate copy | certificates.pdf | 
| hasExpirationDate | xsd:date | 1 | 1 | Date on which the certificate expires | 2122-12-21 | 
| hasRegularAudits | xsd:boolean | 1 | 1 | Is the certificate regularly audited? | true, false | 



# TrustedCloudServiceOfferingContracts

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingContracts
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasPreAvailableContractsForCustomers | xsd:string | 1 | 1 | Are the contracts available to the customer before the closing? | No statement, On request, Published | 
| hasServiceContractLink | xsd:string | 1 | 1 | Is there a link to the service contract? | Freetext | 
| hasApplicableLaw | xsd:string | 1 | 1 | Is there applicable law? | No statement, German contract law, EU contract law | 
| hasAllSubContractorsMentioned | xsd:string | 1 | 1 | Are all involved sub contractors mentioned? | No statement, On request, Yes | 
| hasAllContractConditionsMeetBySubContractor | xsd:string | 1 | 1 | Are all contract conditions transferable to the sub contractors? | No statement, Sub contractors oblige, Sub contractors are bound by contract | 
| hasContractedAccessToCustomerDataAfterBankruptcyOrServiceInterruption | xsd:string | 1 | 1 | Is the access to customer data after the contract has ended, a bankruptcy occurred or other special cases, and is it bound by the contract? | No statement, No rules for data return, Contracted rules to return data | 
| hasContractedRulesToReturnData | xsd:string | 1 | 1 | Contracted rules to return customer data. | Freetext | 
| hasSLAInContract | xsd:string | 1 | 1 | Are there SLAs in the contract? | Yes, No, Maybe | 
| canSLAObservanceBeCheckedByTheCustomer | xsd:boolean | 1 | 1 | Can the SLA observance be checked by the customer? | true, false | 
| hasSLADescription | xsd:string | 1 | 1 | Description of the SLAs | Freetext | 
| hasContractedLegalConsequencesForSLAViolation | xsd:boolean | 1 | 1 | Are there legal consequences for an SLA violation in the contract? | true, false | 
| hasContinuousGuaranteeOfPrivacy | xsd:boolean | 1 | 1 | Is there a contracted continuous guarantee of privacy, integrity and resilience of the systems? | true, false | 
| hasContractedAvailabilityForDataAfterTechnicalProblems | xsd:boolean | 1 | 1 | Are there contracted availabilities for data after a physical or technical problem? | true, false | 
| hasPricingModelSpecifications | xsd:string | 1 | 1 | Are there specifications for the pricing model? | Freetext | 
| hasPublishedPricingModel | xsd:boolean | 1 | 1 | Is the pricing model publicly available to all cutomers? | true, false | 
| hasPricingModelLink | xsd:string | 1 | 1 | Link to the pricing model | Freetext | 
| hasContractRuntimeSpecifications | xsd:string | 1 | 1 | Specifications for the contract runtimes | Freetext | 
| hasTerminationSpecifications | xsd:string | 1 | 1 | Are there specifications for the contract termination and dates? | Freetext | 
| hasFreeTrialPhase | xsd:boolean | 1 | 1 | Is there a free trial phase? | true, false | 
| hasSameDataProtectionAsCommercialService | xsd:boolean | 1 | 1 | Has the free trial phase the same data protection as a commercial service? | true, false | 
| hasContractChangesCommunicatedToCustomer | xsd:string | 1 | 1 | Are changes in the contract communicated to the customer beforehand? | No statement, All changes will be communicated, All changes will be communicated and the customer gets a special termination right | 
| hasRulesForChangeManagementInContract | xsd:string | 1 | 1 | Are rules for change management in the contract? | No statement, Not in the contract, In the contract | 
| hasDescriptionOfChangeManagement | xsd:string | 1 | 1 | Description of change management rules | Freetext | 
| hasObligationToCooperateInContract | xsd:boolean | 1 | 1 | Are all obligations to cooperate or provisions defined in the contract? | true, false | 
| hasCustomerExemptionFromClaimsOfThirdParties | xsd:boolean | 1 | 1 | Is the customer exempt from all claims of third parties while using this service? | true, false | 
| hasContractedRulesForTerminationSupport | xsd:boolean | 1 | 1 | Are there rules defined in the contract for rule termination? | true, false | 
| hasDescriptionForTerminationSupport | xsd:string | 1 | 1 | Description of the rules for termination support | Freetext | 



# TrustedCloudServiceOfferingDataProtection

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingDataProtection
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasGoodImplementationOfDSGVOMeasures | xsd:string | 1 | 1 | How do appropriate measures of the DSGVO measures get implemented? | No statement, Formally described, According to a contract, Certified measurements | 
| hasDetailsAboutDSGVOMeasures | xsd:string | 1 | 1 | Details about the implemented technical and organisational measurements | Freetext | 
| hasFulfillmentOfFormalRequirements | xsd:string | 1 | 1 | Are the predescribed formal requirements fulfilled? | No statement, Requirements are fulfilled, Measurements are documented in the contract, Measurements are documented publicly, Proof via certificate | 
| hasContractedSupportOfDFA | xsd:boolean | 1 | 1 | Has contracted support of a "data protection impact assessment" (DFA), if needed? | true, false | 
| hasContractedPseudonymizationOrEncryptionOfPersonalData | xsd:boolean | 1 | 1 | Is there contracted pseudonymization or encryption of personal data? | true, false | 
| hasContractedImplementationOfDataSubjectRights | xsd:boolean | 1 | 1 | Has contracted implementation of data subject rights? | true, false | 
| hasContractedDeletionOfDataAndLinksInTheCloud | xsd:boolean | 1 | 1 | Has a contracted deletion of files and there links in the cloud? | true, false | 
| hasTypeOfPersonalDataAccordingToTheProvider | xsd:boolean | 1 | 1 | Are declarations in the ADV-contract about personal data as well as categories of the affected persons? | true, false | 
| hasProofsForTheCustomerAccordingToADVContract | xsd:string | 1 | 1 | Which proofs according to the ADV-contract can be given to the customer for examination? | Self-audit, Internal behaviour rules with external proof, Certificate about data protection, Approved behaviour rules, Certificates after Art. 42 DS-GVO, Freetext | 
| hasRegistryAboutCustomerProcessingActivities | xsd:boolean | 1 | 1 | Is a registry about the customers processing activities created according to Art. 30 EU-DSGVO? | true, false | 
| hasPossibleLocationRestrictionForCustomerData | xsd:string | 1 | 1 | Can the hosting of customer data be restricted to a specific location? | No statement, No restriction, Restriction to a region, Restriction to the EU, Restriction to germany | 
| hasPossibleLocationRestrictionForCustomerDataAdministration | xsd:string | 1 | 1 | Can the administration of customer data be restricted to a specific location? | No statement, No restriction, Restriction to a region, Restriction to the EU, Restriction to germany | 
| hasGEORegionOptions | xsd:string | 1 | 1 | Possible GEO-regions | Freetext | 
| hasGuaranteeForImplementationOfDataSubjectRights | xsd:string | 1 | 1 | How can the requirements of the DSGVO according to data subect rights be fulfilled, e.g. the deletion of personal data? | Freetext | 
| hasGuaranteeToDeleteLinksToCustomerData | xsd:string | 1 | 1 | How can it be guaranteed, that also links get deleted, if the data is removed? | Freetext | 
| hasContractedObligationForAllProcessingPersons | xsd:string | 1 | 1 | Is there a contracted obligation for all the people processing personal data according to Art. 28 Abs. 3 S. 2 lit. b, 29, 32 Abs. 4 DSG-VO? | Freetext | 
| hasDataProtectionCertificateName | xsd:string | 1 | 1 | Name of the data protection certificate | Freetext | 
| hasDataProtectionCertificateDetails | xsd:string | 1 | 1 | More details about the certificate e.g. examination, extension and so on | Freetext | 



# TrustedCloudServiceOfferingInteroperability

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingInteroperability
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasPossibilityToAdministerServicesWithStandardizedAPI | xsd:string | 1 | 1 | Is there a possibility to administer services with a standardized API? | No statement, There are no APIs, Web-GUI, Documented APIs, Documented and standardized APIs | 
| hasStandardizedFormatsForVMsAndContainers | xsd:string | 1 | 1 | Does the service use standardized formats virtual machines and containers? | No statement, Proprietary, Open format, Standardized format | 
| hasDescriptionOfServiceStackStandards | xsd:string | 1 | 1 | Description of the standard, the service stack is built on. | Freetext | 
| hasUserAlwaysAccessToCustomerData | xsd:string | 1 | 1 | Does the user always have access to customer data? | No statement, Export can be done after agreement, Export over documented API, Standardized APIs are offered | 
| hasDescriptionAboutDataRepatriation | xsd:string | 1 | 1 | Is there a description about data repatriation and supported file formats? | Freetext | 
| hasStandardizedAPIForServiceIntegration | xsd:string | 1 | 1 | Is there a standardized API to integrate the service into the customers IT-landscape? | No statement, No APIS are offered, Documented APIs, Standardized APIs | 
| hasDescriptionAboutOfferedIntegrationAPIs | xsd:string | 1 | 1 | Description of the offered APIs to integrate the services | Freetext | 
| hasDescriptionOfTechnicalRequirementsForServiceUsage | xsd:string | 1 | 1 | Description for the technical requirements to use the service | Freetext | 
| hasDescriptionOfOrganizationalRequirementsForServiceUsage | xsd:string | 1 | 1 | Description for the organizational requirements to use the service | Freetext | 



# TrustedCloudServiceOfferingOperativeProcesses

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingOperativeProcesses
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasEfficientServiceManagement | xsd:string | 1 | 1 | Is an efficient service management guaranteed (ITIL)? | No statement, Formally described, According to accepted procedure', Certified, Certified and checked regularly | 
| hasDetailsAboutServiceManagement | xsd:string | 1 | 1 | Details about the service management | Freetext | 
| hasServiceManagementCertificateName | xsd:string | 1 | 1 | Service management certificate name | Freetext | 
| hasServiceManagementCertificateDetails | xsd:string | 1 | 1 | More details about the certificate e.g. examination, extension and so on | Freetext | 
| hasAssuredServiceAvailability | xsd:string | 1 | 1 | Assured Service Availability in Percent / Year | No statement, 99.9%, 99.99%, 99.999% | 
| hasMaximumDownTime | xsd:string | 1 | 1 | Maximum downtime of the service in hours | Freetext | 
| hasGuaranteedFastRecoveryOfDataAndConnectionAfterEvent | xsd:string | 1 | 1 | Is a quick recovery of customer data or the connection to the service / data guaranteed after an interruption event? | Freetext | 
| hasDescriptionOfBackupOptions | xsd:string | 1 | 1 | Description of backup options | Freetext | 
| hasGuaranteeForDataProtectionOnBackup | xsd:string | 1 | 1 | Is it guaranteed, that data protection is also extended to the backup? | Freetext | 
| hasPossibilityToProvisionServiceByCustomer | xsd:string | 1 | 1 | Can the customer provision the the service himself? | No statement, Customer cant provision, Customer can provision | 
| hasDescriptionOfSupport | xsd:string | 1 | 1 | Description of the scope of support services | Freetext | 
| hasGuaranteedResponseTimeOnNormalRequests | xsd:string | 1 | 1 | What is the guaranteed response time on normal requests? | No statement, 4 hours, 1 work day, <3 work days | 
| hasGuaranteedResponseTimeOnCriticalRequests | xsd:string | 1 | 1 | What is the guaranteed response time on critical requests? | No statement, 4 hours, 1 work day, <3 work days | 
| hasAverageTimeUntilResolvanceOfMinorProblems | xsd:string | 1 | 1 | Whats the average time until minor problems are resolved? | No statement, <1 work day, 1 work day, <4 work days | 
| hasAverageTimeUntilResolvanceOfMajorProblems | xsd:string | 1 | 1 | Whats the average time until major problems are resolved? | No statement, <1 work day, 1 work day, <4 work days | 
| hasAvailabilityOfSupport | xsd:string | 1 | 1 | What is the availability of the support? | No statement, 5x8, 24/7 | 
| hasCompleteUserDocumentation | xsd:string | 1 | 1 | Is there a complete user documentation? | No statement, Complete and updated, Regularly updated, Updated version online | 
| hasCompleteSystemDocumentation | xsd:string | 1 | 1 | Is there a complete system documentation? | No statement, Complete and updated, On request | 
| hasOfferForTraining | xsd:boolean | 1 | 1 | Is there an offer for training? | Yes, No | 
| hasSpecificationFromTrainingPartners | xsd:string | 1 | 1 | Specification from training partners | Freetext | 



# TrustedCloudServiceOfferingSecurity

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingSecurity
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasEfficientManagementForInformationSafety | xsd:string | 1 | 1 | Is there an efficient management for information safety given, e.g. certification according to ISO 27001? | No statement, According to accepted procedure, Certified, Certified and checked regularly | 
| hasDetailsAboutProcess | xsd:string | 1 | 1 | More details about the process in the IT safety management | Freetext | 
| hasDescriptionOfTheProcessToReportDataLeaks | xsd:string | 1 | 1 | Description of the process to report data leaks to the customer | Freetext | 
| hasSecurityCertificateName | xsd:string | 1 | 1 | Certificate name for the security certificate | Freetext | 
| hasSecurityCertificateDetails | xsd:string | 1 | 1 | More details about the certificate e.g. examination, extension and so on | Freetext | 
| hasDataCenterCertificateName | xsd:string | 1 | 1 | Certificate name for the data center or infrastructure certificate | Freetext | 
| hasDataCenterCertificateDetails | xsd:string | 1 | 1 | More details about the certificate e.g. examination, extension and so on | Freetext | 
| hasEncryptionTypesForDataTransmissionAndStorage | xsd:string | 1 | 1 | Encryption types that can be used to encrypt data for transmission and storage? | Freetext | 
| hasKeyManagementOptions | xsd:string | 1 | 1 | Key management options | No statement, Key are set by the provider, Keys are set by the customer, Keys can be set by external services | 
| hasRightsAndRoleConcept | xsd:string | 1 | 1 | Which rights- and role-concept are used? | No statement, User-wide right- and role-concept, Others | 



# TrustedCloudServiceOfferingSubCompanies

```mermaid
classDiagram

ServiceOffering <|-- TrustedCloudServiceOffering 
TrustedCloudServiceOffering <|-- TrustedCloudServiceOfferingSubCompanies
```

 **Super class**: [TrustedCloudServiceOffering](#trustedcloudserviceoffering)

| Title | Data Type | Min Count | Max Count | Description | Example Value | 
| ------- | ------- | ------- | ------- | ------- | ------- | 
 | hasSubCompanyName | xsd:string | 1 | 1 | Name of the sub company or owner of the data center | DataCenter Inc. | 
| hasLegalFormOfSubCompany | xsd:string | 1 | 1 | Legal form of the sub company or owner of the data center | Inc. | 
| hasRegisterNumberOfSubCompany | xsd:string |  0 |  unlimited | Register number of the sub company or owner of the data center | Inc. | 
| hasMainAddressOfSubCompany | xsd:address | 1 | 1 | Address of the sub company or owner of the data center | Address | 
| hasDataCenterLocation | xsd:string | 1 | 1 | Location to at least federal level of the data center | Hessen, Germany | 
| hasIsIndependentSubCompany | xsd:string | 1 | 1 | Is the data center company internal or his legally sovereign company? | Company internal, Legally sovereign company | 
| hasSubCompanyLogo | xsd:string |  0 |  unlimited | Logo of the sub company or data center | logo.png | 
| hasCompanyWebsite | xsd:string |  0 |  unlimited | Website of the sub company or data center | http://acme2.com | 
| hasNumberOfEmployees | xsd:integer |  0 |  unlimited | Number of employees working for the company | 40 | 
| hasSalesVolume | xsd:integer |  0 |  unlimited | Sales volume of the company | 30 | 
| hasCompanyCategory | xsd:string | 1 | 1 | Category of the company | Reseller, ISV, CSP, Others | 
| hasNumberOfPublicCloudServices | xsd:string |  0 |  unlimited | Number of publicly offered cloud services | 12 | 
| hasExperienceLevelForCloudServices | xsd:string |  0 |  unlimited | Experience level of this provider to provide cloud services. | No statement, <1 year, 1-5 years, >5 years | 



