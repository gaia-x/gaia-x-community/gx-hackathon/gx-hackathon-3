# GXFS Track

Below, you find the list of session including a link describing session results:


- [Verifiable Credentials & Policies for Automated Decisions](results/verifiable_creds_and_policies.md) 
- [Status Quo GXFS-DE project and exchange](gxfs-track/results/status_quo_gxfs_de.md)
- [SD Tooling and Service Characteristics](gxfs-track/results/sd_tooling.md)
- [Self Description Cataloging and Life Cycle](gxfs-track/results/sd_cataloging.md)


