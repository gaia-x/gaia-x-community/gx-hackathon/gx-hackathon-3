# Results of Session "SD Tooling and Service Characteristics"

## Session's Task

- Participants write own self-descriptions for their services using the tools and support from experts.
- Goals:
    1. Have more examples
    2. validate current self-description schema and identify new requirements or open issues for the self-description schema.

 
## Session's Results

### New Self-Description examples
Self-description JSON-LS files can be found in [selfgxfs-track/self-descriptions](gxfs-track/self-descriptions) folder.

#### IaaS Offering 

ACME wants to offer a Infrastructure as a Service IaaS on Gaia-X Marketplace. IaaS depends on three sub-services: (1) storage, (2) compute and (3) networking. IaaS is running on ACME's own data center located in Germany.


| Name | Taxonomy Element | relartion to Servie Offering | SD File |
|--|--|--|--|
| IaaS | Infrastructure Service Offering | Top-level Service Offering  | [iaas_service.json](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-3/-/blob/main/gxfs-track/results/iaas_service.json) | 
| Compute | Compute Service Offering  | depending Service Offering | [nova_IaaS.json](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-3/-/blob/main/gxfs-track/results/nova_IaaS.json) | 
| Storage | Storage Service Offering  | depending Service Offering | [cinder_IaaS.json](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-3/-/blob/main/gxfs-track/results/cinder_IaaS.json) | 
| Networking | Networking Service Offering  | depending Service Offering | [neutron_IaaS.json](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-3/-/blob/main/gxfs-track/results/neutron_IaaS.json) |
| Data Center | Bare Metal Node   | composing Resource  | [data_center.json](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-3/-/blob/main/gxfs-track/results/data_center.json) |


#### Foo Service Offering 

...

## Requirements/Input to SDs

- Model Processings as a service offering
- Model sustainability, security, privacy, confidential computing as well as performance options
- Deduce high level (Gaia-X) labels with respect to sustainability, security, privacy, confidential computing as well as performance features 


```mermaid
classDiagram

ServiceOffering <|-- Software

Software <|-- DataServiceOffering

Software <|-- ProcessingServiceOffering
ProcessingServiceOffering --> "1..*" SoftwareResource : "uses" 
ProcessingServiceOffering --> "1..*" Node : "runsOn" 


ProcessingServiceOffering <|-- StatefulProcessing
ProcessingServiceOffering <|-- StatelessProcessing

```

### Attributes of Software Resource (algorithm)
- copyright owner
- hardward architecture run on
- environment (os, programming library (python, java)) 
- decription of Input and output (existing standard, like OData, openAPI) (composed attribute link data fromat and data format standard)
- hardware requiments (GPU, ...)
- performance capabilities


### Attributes of Infrastructure as a Service

- quality of service attributes (standard metrics)
- high-availability capabilities
- desater recovery
- security attributes (confidential computing, encryption)
- we need more than one attribute to describe confidential computing

### Attrbibutes of Bare Metal Nodes and Hardware

- Storage as a Service --> (Storage) Node (attribute hardrive_encrypted=yes/no)
- Node --> HardDrive (additional attribute encryption=yes/no)


## Tool chain - Requirements:

- Link to documentation, tooltip, ...
- Complete tool chain linked/integrated to VC process, cataloque and protal
- Automation: discovery of SDs, create SDs automatically, ... 
