# Overview of Hackathon #3

We are delighted to invite the Gaia-X community to the third official Gaia-X Hackathon. **The Hackathon will take place on the 28th and 29th of March 2022.**

The Gaia-X Hackathon is a two days event with each day being crowned with a presentation session of showcases that leverage components and concepts of the Gaia-X architecture. If you would like to participate in the Hackathon, please **register** via the following link:

https://online2.superoffice.com/Cust26633/CS/scripts/customer.fcgi?action=formFrame&formId=F-buhKosWT

The goals of the Gaia-X Hackathon are

1. to increase the technical competence and knowledge related to technology relevant to the Gaia-X ecosystem within the community and
1. to integrate and align different ideas, concepts, pilots and prototypes to consistent approaches.

The Gaia-X Hackathon will be organized as a community event in regular intervals by the Open Work Package Minimal Viable Gaia-X/Piloting.

# Getting in Touch

For discussions related to the Hackathon you can use the mailing list of the MVG/Piloting Open Work Package. You can subscribe to it via the following link:
https://list.gaia-x.eu/postorius/lists/wp-minimal-viable-gaia.list.gaia-x.eu/

**If you would like to participate in the organization and/or would like to get updates on the organization, please join our call on:**
- Thursday 9 a.m. CET - join: [MS Teams Link](https://teams.microsoft.com/l/meetup-join/19%3ameeting_NWIwNTVmOTEtMDIwNi00N2RjLTgxOTUtZWVhYTkzNTgwZmEy%40thread.v2/0?context=%7b%22Tid%22%3a%22e3386221-1143-4129-acd3-cfcb8a6fcc7f%22%2c%22Oid%22%3a%220838870d-2d9b-49c1-a396-4cdb76099e06%22%7d)


# Schedule and Further Information

You'll find the **schedule** and wiki right [here](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-3/-/wikis/GX-Hackathon-3).
